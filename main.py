"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import requests
import numpy as np
import json
import flask
from flask import Flask, render_template, Response, request, redirect
import cv2
import argparse
import time
import logging
from src.read_stream import ReadFromRTSP, ReadFromRTMP
from src.argconfig import ArgConf
from src.const import *
from src.model_classes import get_model_n_class_list
from src.camera import get_camera_object
from src.inference import send_for_inference, start_grpc_connection
from src.preprocess_frame import preprocess_frame
from src.postprocess import postprocess_results
from src.cloud_agent import AwsAgent
from src.logger import get_logger

ap = argparse.ArgumentParser()
args = ArgConf(ap)

app = Flask(__name__)
print(__name__)


model, names = get_model_n_class_list(args)

# Create logger for various debugging logs.
logger = get_logger()



camera = get_camera_object(args)
if not camera.isOpened():
    error_message = "Could not open camera. Please check camera or rtsp path"
    logger.error(error_message)
    camera.release()
    raise Exception(error_message)

cloud_stream = None
if args["cloud_rtmp_stream"] != "":
    cloud_stream = ReadFromRTMP(args["cloud_rtmp_stream"])
    cloud_stream.start()

frame_count = 0
frame_read_time_avg = 0
preprocess_time_avg = 0
inference_time_avg = 0
postprocess_time_avg = 0
cloud_metrics_time_avg = 0
pure_inf_time_avg = 0
rest_inf_time_avg = 0
normal_cam_read = 0
pipeline_time_avg = 0
pure_inf_time = 0

person1 = 0
bicycle1 = 0
car1 = 0
bus1 = 0
truck1 = 0
rest_inf_time1 = 0
pure_inference_time1 = 0
total_count1 = 0


def on_message_received(topic, payload, dup, qos, retain, **kwargs):
    logger.info("Received message from topic '%s': %s", topic, payload)
    payload_str = payload.decode("utf-8")
    logger.info("Received payload type: %s", type(payload_str))                                                                                                                                                       
    command = str(payload.decode("utf-8"))  
    jsonMsg  = json.loads(command)    
    logger.info("Received JSON message type: %s", type(jsonMsg))
    logger.info("Received command: %s", jsonMsg)
    global person1, truck1, bicycle1, bus1, car1, total_count1, pure_inference_time1, rest_inf_time1
    person1 = int(jsonMsg["person"])
    logger.info("Person.... %s",person1)
    car1 = int(jsonMsg["car"])
    bicycle1 = int(jsonMsg["bicycle"])
    truck1 = int(jsonMsg["truck"])
    bus1 = int(jsonMsg["bus"])
    total_count1 = int(jsonMsg["total_count"])
    pure_inference_time1 = float(jsonMsg["Pure_inf"])
    rest_inf_time1 = float(jsonMsg["End_to_end_FPS"])


if args["cloud_service"]=="aws":

    cloud_agent = AwsAgent()

    cloud_conf_file = open("config/cloud_credential.json", "r")
    load_cloud_conf = json.load(cloud_conf_file)

    topic = load_cloud_conf["pub_topic"]

    subscribe_future, packet_id = cloud_agent.conn_handler.subscribe(topic=topic, qos=cloud_agent.qos, callback=on_message_received)            
    subscribe_result = subscribe_future.result()    
    logger.info("Subscribed with %s", str(subscribe_result['qos']))

def no_ui_inference():
    """
    Perform inference on video frames without displaying a UI.

    This function reads video frames, processes them through an inference pipeline,
    and provides various metrics and results without a graphical user interface.

    Returns:
        None
    """
    global person, bicycle, car, bus, truck, total_count
    global rest_inf_time, pure_inference_time, pipeline_time
    global pipeline_time_avg, normal_cam_read, frame_read_time_avg, preprocess_time_avg, inference_time_avg, postprocess_time_avg, cloud_metrics_time_avg, frame_count, pure_inf_time_avg, rest_inf_time_avg
    inference_conn = start_grpc_connection(args["ipaddr"], args["inf_port"])
    while (camera.isOpened()):
        start = time.time()
        ret, frame = camera.read()  
        normal_cam_read += time.time() - start    
        frame_read_time = camera.frame_read_time

        if frame is None:
            logger.error('End of video/video not detected')
            raise Exception('NoFrameAvailableError')

        prepro_start_time = time.time()
        img = preprocess_frame(frame, model)
        preprocess_time = time.time() - prepro_start_time

        inf_start = time.time()
        right_boxes, right_classes, right_scores, model_load_time, pure_inference_time = send_for_inference(
            img, inference_conn)
        inference_time = time.time() - inf_start

        postpro_start = time.time()
        arr, img = postprocess_results(
            right_boxes, right_classes, right_scores, frame, img, names,  model)

        person, bicycle, car, bus, truck= arr[0], arr[1], arr[2], arr[3], arr[4]
        total_count = arr[0] + arr[1] + arr[2] + arr[3] + arr[4]
        
        postprocess_time = time.time() - postpro_start

        if args["cloud_address"] != "":
            cloud_info_start = time.time()
            image_string = cv2.imencode('.jpg', img)[1]
            image_tensor = image_string.tolist()
            iot_resp = {"cloud_metrics":{"person": person, "bicycle": bicycle, "car": car, "bus": bus, "truck": truck,
                        "pure_fps": pure_inference_time, "end_to_end_fps": rest_inf_time, "total_count": total_count}, 
                        "image": image_tensor,
                        "app_time": {"Start_time": 0, "Cloud_start": 0}}
            resp = requests.post("http://"+ args["cloud_address"] +"/video",json=iot_resp)
            resp = json.loads(resp.text)
            cloud_metrics_time =  resp["End_to_End_pipeline_time"] - cloud_info_start
            rest_inf_time = 1 / (resp["End_to_End_pipeline_time"] - start)
            pipeline_time = resp["End_to_End_pipeline_time"] - start
        else: 
            end = time.time()
            rest_inf_time = 1 / (end - start)
            pipeline_time = end - start
        
        frame_count += 1
        if frame_count % 10 == 0:    
            logger.info("--------------------------TIME TAKEN REPORT-------------------------------")
            logger.info("Video reading thread :%s ", frame_read_time_avg / 10)
            logger.info("Preprocess time    :%s ", preprocess_time_avg / 10)
            logger.info("Inference time     :%s ", inference_time_avg / 10)
            logger.info("Postprocess time   :%s ", postprocess_time_avg / 10)
            frame_count = 0
            frame_read_time_avg = 0
            preprocess_time_avg = 0
            inference_time_avg = 0
            postprocess_time_avg = 0
            normal_cam_read = 0

            if args["cloud_address"] != "":
                logger.info("Cloud metrics time : %s",
                            cloud_metrics_time_avg / 10)
                cloud_metrics_time_avg = 0
            logger.info("Total time taken   : %s", pipeline_time_avg / 10)
            logger.info("END_TO_END_FPS     : %s", rest_inf_time_avg / 10)
            logger.info("Pure inference FPS : %s", pure_inf_time_avg / 10)
            rest_inf_time_avg = 0
            pure_inf_time_avg = 0
            pipeline_time_avg = 0
            logger.info(
                "-----------------------------END-------------------------------------------")
        else:
            frame_read_time_avg += frame_read_time
            preprocess_time_avg += preprocess_time
            inference_time_avg += inference_time
            postprocess_time_avg += postprocess_time
            pipeline_time_avg += pipeline_time
            if args["cloud_address"] != "":
                cloud_metrics_time_avg += cloud_metrics_time
            rest_inf_time_avg += rest_inf_time
            pure_inf_time_avg += pure_inference_time
        logger.info("--------------------Inference results-------------------")
        logger.info("Detected objects : ")
        logger.info(
            f"Person : {person}, Car:{car}, Bicycle: {bicycle}, Truck: {truck}, Bus: {bus}")
        logger.info("FPS stats :")
        logger.info(
            f"Pure inference time: {pure_inference_time}, End to End Inference time: {rest_inf_time}")


def gen_frames():
    """
    Generate video frames with inference results for streaming.
    """
    global person, bicycle, car, bus, truck
    global rest_inf_time, pure_inference_time, total_count, pipeline_time
    global pipeline_time_avg, normal_cam_read, frame_read_time_avg, preprocess_time_avg, inference_time_avg, postprocess_time_avg, cloud_metrics_time_avg, frame_count, pure_inf_time_avg, rest_inf_time_avg
    inference_conn = start_grpc_connection(args["ipaddr"], args["inf_port"])

    while(camera.isOpened()):

        start = time.time()
        ret, frame = camera.read()  
        normal_cam_read += time.time() - start    
        frame_read_time = camera.frame_read_time

        if frame is None:
            logger.error('End of video/video not detected')
            exit()

        prepro_start_time = time.time()
        img = preprocess_frame(frame, model)
        preprocess_time = time.time() - prepro_start_time
        
        inf_start = time.time()
        right_boxes, right_classes, right_scores, model_load_time, pure_inference_time = send_for_inference(img, inference_conn)
        inference_time = time.time() - inf_start

        postpro_start = time.time()
        arr, img = postprocess_results(right_boxes, right_classes, right_scores, frame, img, names,  model)
        
        if cloud_stream is None:
            image_string = cv2.imencode('.jpg', img)[1]
            image_tensor = image_string.tobytes()
            yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + image_tensor + b'\r\n')
        else:
            ret, cloud_frame = cloud_stream.read()
            if not(cloud_frame is None):
                cl_image_str = cv2.imencode('.jpg', cloud_frame)[1]
                cl_image_byt = cl_image_str.tobytes()
                yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + cl_image_byt + b'\r\n')

        person, bicycle, car, bus, truck= arr[0], arr[1], arr[2], arr[3], arr[4]
        total_count = arr[0] + arr[1] + arr[2] + arr[3] + arr[4]
        
        postprocess_time = time.time() - postpro_start
                
        if args["cloud_address"] != "":
            cloud_info_start = time.time()
            image_string = cv2.imencode('.jpg', img)[1]
            
            image_tensor = image_string.tolist()
       
            iot_resp = {"cloud_metrics":{"person": person, "bicycle": bicycle, "car": car, "bus": bus, "truck": truck,
                        "pure_fps": pure_inference_time, "end_to_end_fps": rest_inf_time, "total_count": total_count}, 
                        "image": image_tensor,
                        "app_time": {"Start_time": 0, "Cloud_start": 0}}
            
            resp = requests.post("http://"+ args["cloud_address"] +"/video",json=iot_resp)
            resp = json.loads(resp.text)
            cloud_metrics_time = time.time() - cloud_info_start
            rest_inf_time = 1 / (resp["End_to_End_pipeline_time"] - start)
            pipeline_time = resp["End_to_End_pipeline_time"] - start
        else: 
            end = time.time()
            rest_inf_time = 1 / (end - start)
            pipeline_time = end - start
        
        frame_count += 1
        if frame_count % 10 == 0:
            logger.info(
                "--------------------------TIME TAKEN REPORT-------------------------------")
            logger.info("Preprocess time    : %s", preprocess_time_avg / 10)
            logger.info("Inference time     : %s", inference_time_avg / 10)
            logger.info("Postprocess time   : %s", postprocess_time_avg / 10)
            frame_count = 0
            frame_read_time_avg = 0
            preprocess_time_avg = 0
            inference_time_avg = 0
            postprocess_time_avg = 0
            normal_cam_read = 0

            if args["cloud_address"] != "":
                logger.info("Cloud metrics time : %s",
                            cloud_metrics_time_avg / 10)
                cloud_metrics_time_avg = 0
            logger.info("Total time taken   : %s", pipeline_time_avg / 10)
            logger.info("END_TO_END_FPS     : %s", rest_inf_time_avg / 10)
            logger.info("Pure inference FPS : %s", pure_inf_time_avg / 10)
            rest_inf_time_avg = 0
            pure_inf_time_avg = 0
            pipeline_time_avg = 0
            logger.info(
                "-----------------------------END-------------------------------------------")
        else:
            frame_read_time_avg += frame_read_time
            preprocess_time_avg += preprocess_time
            inference_time_avg += inference_time
            postprocess_time_avg += postprocess_time
            pipeline_time_avg += pipeline_time
            if args["cloud_address"] != "":
                cloud_metrics_time_avg += cloud_metrics_time
            rest_inf_time_avg += rest_inf_time
            pure_inf_time_avg += pure_inference_time

if args["cloud_address"] == "aws":
    # Sending all the metrics to the WEB UI using flask
    @app.route('/total_count')
    def total_count():
        def update():
            global total_count1
            yield f'data: {total_count1}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/learn_1d')
    def learn_1d():
        def update():
            global person1
            yield f'data: {person1}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/learn_2d')
    def learn_2d():
        def update():
            global car1
            yield f'data: {car1}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/learn_3d')
    def learn_3d():
        def update():
            global bus1
            yield f'data: {bus1}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/learn_4d')
    def learn_4d():
        def update():
            global truck1, prev_count, frame_count
            yield f'data: {truck1}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/learn_5d')
    def learn_5d():
        def update():
            global bicycle1, prev_count, frame_count
            yield f'data: {bicycle1}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/rest_fps')
    def rest_fps():
        def update():
            global rest_inf_time1
            yield f'data: {rest_inf_time1:.3f}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/pure_fps')
    def pure_fps():
        def update():
            global pure_inference_time1
            yield f'data: {pure_inference_time1:.3f}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


else:

    @app.route('/total_count')
    def total_count():
        def update():
            global total_count
            yield f'data: {total_count}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')
    @app.route('/learn_1d')
    def learn_1d():
        def update():
            global person
            yield f'data: {person}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/learn_2d')
    def learn_2d():
        def update():
            global car
            yield f'data: {car}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/learn_3d')
    def learn_3d():
        def update():
            global bus
            yield f'data: {bus}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/learn_4d')
    def learn_4d():
        def update():
            global truck1, prev_count, frame_count
            yield f'data: {truck}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/learn_5d')
    def learn_5d():
        def update():
            global bicycle, prev_count, frame_count
            yield f'data: {bicycle}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/rest_fps')
    def rest_fps():
        def update():
            global rest_inf_time
            yield f'data: {rest_inf_time:.3f}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')


    @app.route('/pure_fps')
    def pure_fps():
        def update():
            global pure_inference_time
            yield f'data: {pure_inference_time:.3f}\n\n'
        return flask.Response(update(), mimetype='text/event-stream')

@app.route('/video_feed')
def video_feed():
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')


# Function to render the HTML page in the WEB Browser.... The user can select the number of devices to be added to the web ui using below code snippets
@app.route('/')
def index():
    """Video streaming home page."""
    if args["cloud_address"] != "" and args["cloud_service"]=="alibaba" and args["linkvisual"]=="": 
        print("Alibaba")
        return render_template('index.html', device_1=device_1, device_2=device_2, link_1=dev_link_1, link_2=dev_link_2, cloud_service=args["cloud_service"])

    if args["cloud_address"] != "" and args["cloud_service"]=="alibaba" and args["linkvisual"]=="on":
        print("LinkVisual") 
        # For opening the WEB UI with only one tab for one device uncomment the below return code.
        # Note that the variables for 2rd device should be uncommented in line number 60 and 61 and an entry should be created in config/config.json for execution without any errors.
        # return render_template('index.html', device_1=device_1, link_1= dev_link_1)

        # For adding 2nd device uncomment the below return code.
        # Note that the variables for 2rd device should be uncommented in line number 60 and 61 and an entry should be created in config/config.json for execution without any errors.
        return render_template('index3.html', device_1=device_1, device_2=device_2, link_1=dev_link_1, link_2=dev_link_2, cloud_service=args["cloud_service"])

        # For adding 3rd device uncomment the below return code.
        # Note that the variables for 3rd device should be uncommented in line number 60 and 61 and an entry should be created in config/config.json file for execution without any errors.
        # return render_template('index.html', device_1=device_1, device_2=device_2, device_3=device_3, link_1= dev_link_1 , link_2=dev_link_2, link_3=dev_link_3)

        # For adding the 4th device uncomment the below return code.
        # Note that the variables for 3rd device and 4th device should be uncommented from line number 65 to 66 and an entry should be created in config/config.json file for execution without any errors.
        # return render_template('index.html', device_1=device_1, device_2=device_2, device_3=device_3, device_4=device_4, link_1= dev_link_1 , link_2=dev_link_2, link_3= dev_link_3, link_4= dev_link_4)
    elif args["cloud_address"] != "" and args["cloud_service"]=="aws":
        kvs_config_file = open("config/kvs_credential.json")
        kvs_new_config = json.load(kvs_config_file)

        return render_template('index2.html', accesskey=kvs_new_config["accessKey"],secretKey=kvs_new_config["secretKey"],region=kvs_new_config["region"],streamName=kvs_new_config["streamName"],device_1=device_1, device_2=device_2)
        # For opening the WEB UI with only one tab for one device uncomment the below return code.
        # Note that the variables for 2rd device should be uncommented in line number 60 and 61 and an entry should be created in config/config.json for execution without any errors.
        # return render_template('index1.html', device_1=device_1, link_1= dev_link_1)

        # For adding 2nd device uncomment the below return code.
        # Note that the variables for 2rd device should be uncommented in line number 60 and 61 and an entry should be created in config/config.json for execution without any errors.
        # return render_template('index2.html', device_1=device_1, device_2=device_2, link_1=dev_link_1, link_2=dev_link_2)

        # For adding 3rd device uncomment the below return code.
        # Note that the variables for 3rd device should be uncommented in line number 60 and 61 and an entry should be created in config/config.json file for execution without any errors.
        # return render_template('index1.html', device_1=device_1, device_2=device_2, device_3=device_3, link_1= dev_link_1 , link_2=dev_link_2, link_3=dev_link_3)

        # For adding the 4th device uncomment the below return code.
        # Note that the variables for 3rd device and 4th device should be uncommented from line number 65 to 66 and an entry should be created in config/config.json file for execution without any errors.
        # return render_template('index1.html', device_1=device_1, device_2=device_2, device_3=device_3, device_4=device_4, link_1= dev_link_1 , link_2=dev_link_2, link_3= dev_link_3, link_4= dev_link_4)
    else:

        print("Local Streaming") 

        # For opening the WEB UI with only one tab for one device uncomment the below return code.
        # Note that the variables for 2rd device should be uncommented in line number 60 and 61 and an entry should be created in config/config.json for execution without any errors.
        # return render_template('index.html', device_1=device_1, link_1= dev_link_1)

        # For adding 2nd device uncomment the below return code.
        # Note that the variables for 2rd device should be uncommented in line number 60 and 61 and an entry should be created in config/config.json for execution without any errors.
        return render_template('index1.html', device_1=device_1, device_2=device_2, link_1=dev_link_1, link_2=dev_link_2, cloud_service=args["cloud_service"])


@app.route('/video_server')                                                                                                                           
def video_server():                                                                                                                                 
    return redirect("http://" + args["cloud_address"])

if __name__ == '__main__':
    if args["disable_ui"]:                                                                                                      
        app.run(host="0.0.0.0")
    else:      
        no_ui_inference()   